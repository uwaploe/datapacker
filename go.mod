module bitbucket.org/uwaploe/datapacker

require (
	bitbucket.org/uwaploe/go-covis v1.21.0
	bitbucket.org/uwaploe/go-reson v1.2.3 // indirect
	github.com/garyburd/redigo v1.6.0
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.4.0
)
