FROM alpine:latest
MAINTAINER Mike Kenney <mikek@apl.uw.edu>

RUN apk add --no-cache tar

RUN mkdir /app
WORKDIR /app
COPY datapacker /app
ENTRYPOINT ["./datapacker"]
